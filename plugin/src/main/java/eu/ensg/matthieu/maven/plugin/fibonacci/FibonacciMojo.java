package eu.ensg.matthieu.maven.plugin.fibonacci;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.*;
import eu.ensg.matthieu.fibonacci.core.Fibonacci;

@Mojo(name="fibonacci")
public class FibonacciMojo extends AbstractMojo {

	@Parameter(property="rank")
	private int rank;

	@Override
	public void execute() {
		System.out.println(Fibonacci.rank(this.rank));
	}

	// public static int fibonacci (int rank) {
	// 	if (rank <= 1) {
	// 		return 0;
	// 	}
	// 	int a = 0;
	// 	int b = 1;
	// 	int result = 0;
	// 	for (int i = 0; i < rank-1; i++) {
	// 		result = a + b;
	// 		a = b;
	// 		b = result;
	// 	}
	// 	return result;
	// }
}
