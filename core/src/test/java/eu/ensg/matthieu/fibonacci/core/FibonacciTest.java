package eu.ensg.matthieu.fibonacci.core;

import org.junit.Assert;
import org.junit.Test;

public class FibonacciTest {

	@Test
    public void rankTest() {
	    Assert.assertEquals(0, Fibonacci.rank(0));
	    Assert.assertEquals(0, Fibonacci.rank(Integer.MIN_VALUE));
	    Assert.assertEquals(0, Fibonacci.rank(-1));
	    Assert.assertEquals(0, Fibonacci.rank(-15));
	    Assert.assertEquals(1, Fibonacci.rank(1));
	    Assert.assertEquals(1, Fibonacci.rank(2));
	    Assert.assertEquals(5, Fibonacci.rank(5));
	    Assert.assertEquals(233, Fibonacci.rank(13));
	}
		
}