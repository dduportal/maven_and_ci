package eu.ensg.matthieu.springboot;

import eu.ensg.matthieu.fibonacci.core.Fibonacci;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.*;

@Controller
public class FibonacciController {

//     @RequestMapping(value = "/fibonacci", method = RequestMethod.GET)
    @CrossOrigin
    @GetMapping("/fibonacci")
    public String fibonacciController(@RequestParam int rank) {
        System.out.println(Fibonacci.rank(rank));
        return Integer.toString(Fibonacci.rank(rank));
    }
}
