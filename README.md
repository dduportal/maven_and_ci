# Plugin Maven Fibonacci


Pour utiliser le plugin, cloner le dossier, puis se placer dans le dossier en ligne de commande.

```
mvn clean install
```

Pour calculer le 13ème terme de la suite de Fibonacci, entrer la commande:
```
mvn eu.ensg.matthieu.fibonacci.plugins:fibonacci-maven-plugin:1.0.0-SNAPSHOT:fibonacci -Drank=13
```
Le SpringBoot fait partie du projet, mais je n'ai pas trouvé de façon pour le lancer autrement qu'en me plaçant dans le dossier SpringBoot pour exécuter la commande suivante:
```
mvn spring-boot:run
```
Et même dans ce cas-là, je n'ai pas réussi à trouver la façon de faire retourner le résultat via l'URL suivante, que j'ai pourtant configuré et qui affiche le résultat dans la console SpringBoot lorsqu'il est appelé:
```
localhost:8080/fibonacci?rank=13
```


# Intégration continue du projet

J'ai galéré sur la mise en place du système d'intégration (je n'avais pas vu qu'il existait des templates pour les .gitlab-ci.yml, donc j'ai dû configurer un Kubernetes engine chez Google (l'avantage c'est que j'ai vraiment compris comment tout fonctionne maintenant)).
La documentation du .gitlab-ci.yml est en commentaires de ce fichier.
Je n'ai pas pû m'occuper de l'intégration des sites des différentes branches, faute de temps.