package eu.ensg.matthieu.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;
import eu.ensg.matthieu.fibonacci.core.Fibonacci;

@SpringBootApplication
public class FibonacciSpringbootApplication {

	public static void main(String[] args) {
	    SpringApplication.run(FibonacciSpringbootApplication.class, args);
	}
	

}

