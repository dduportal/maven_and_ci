package eu.ensg.matthieu.fibonacci.core;


public class Fibonacci {

    public static int rank(int rank) {
        int n0 = 0;
        int n1 = 1;
        
        if (rank < 1) return n0;
		else if (rank == 1) return n1;

		int n2 = 0;
		for (int i = 0; i < rank-1; i++) {
			n2 = n0 + n1;
			n0 = n1;
			n1 = n2;
		}
		return n2;
    }
}